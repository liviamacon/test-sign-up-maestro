import random
import os
from dotenv import load_dotenv


class Credential:

    load_dotenv()
    url_botmaestro = os.getenv("URL")
    key_botmaestro = os.getenv("KEY")
    workspace_botmaestro = os.getenv("WORKSPACE_MAESTRO")

    lower = "abcdefghijklmnopqrstuvxywz"
    upper = "ABCDEFGHIJKLMNOPQRSTUVXYWZ"
    numbers = "123456789"

    all = lower + upper + numbers
    length = 8
    password_signup = "".join(random.sample(all, length))

    def __init__(self, email=os.getenv("EMAIL"), password_email=os.getenv("PASSWORD"),
                 site_form=os.getenv("SITE_FORM"), name=os.getenv("NAME"), surname=os.getenv("SURNAME"),
                 workspace=os.getenv("WORKSPACE"), login=os.getenv("LOGIN"), password_signup=password_signup):

        self.email = email
        self.password_email = password_email
        self.site_form = site_form
        self.name = name
        self.surname = surname
        self.workspace = workspace
        self.login = login
        self.password_signup = password_signup
        self.confirm_pass = password_signup










