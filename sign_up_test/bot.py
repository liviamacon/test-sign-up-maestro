import os
from botcity.web import WebBot, Browser, BROWSER_CONFIGS
from botcity.maestro import *
from sign_up_test.base_module import TestSignUp, FetchEmail
from credential import Credential
from dotenv import load_dotenv
import requests
import datetime
import sys



class TestSignUpGo(WebBot):

    def action(self, execution=None):

        try:

            # Starts BotMaestro
            self.maestro = BotMaestroSDK()
            self.maestro.login(Credential().url_botmaestro, Credential().workspace_botmaestro,
                               Credential().key_botmaestro)


            # # Fetch task id for execution in action
            task_id = execution.task_id
            print(task_id)

            # link form for create account
            self.link_forms_Botcity = (Credential().site_form)

            # Init class test and Fetch Email
            self.page_test = TestSignUp()
            self.page_activate_account = FetchEmail()

            # Delete Account
            self.page_test.delete_account()

            # Open form account into browser
            self.page_test.open_brws(self.link_forms_Botcity)


            # collecting data for fill form
            self.page_test.elements_for_forms()

            # return False if not found image for click
            self.not_found = self.page_test.fill_forms()
            print(self.not_found)
            if self.not_found == "False":
                return False


        except Exception as err:
            print("Error when trying to launch the browser")

            # Create a new log entry of login failed
            self.maestro.new_log_entry(activity_label="sign_up_test",
                                       values={"timestamp": datetime.datetime.now().strftime("%d/%m/%Y-%H:%M"),
                                               "conclusion": "FAIL",
                                               "note": "The test ended without starting the steps."
                                                       ", check if chromedriver has been updated."})

            # Create alert login failed
            self.maestro.alert(task_id=task_id, title="Error when trying to launch the browser",
                               message="The test ended without starting the steps, check if chromedriver has been updated.",
                               alert_type=AlertType.ERROR)

            # Send email alerting "ERROR"
            self.subject = "Test ERROR"
            self.body = "The test ended without starting the steps, check if chromedriver has been updated."
            self.page_activate_account.send_email(self.subject, self.body)

            self.stop_browser()

            # Finish task
            self.maestro.finish_task(task_id=task_id, status=AutomationTaskFinishStatus.FAILED,
                                     message="The test ended without starting the steps, "
                                             "check if chromedriver has been updated.")

            return False


        try:
            # Fetch e-mail activate account and open the link

            self.page_activate_account.login()
            # Wait high to email arrive
            self.wait(60000)
            self.body_email = self.page_activate_account.confirm_email()

            # If not found email in first minute
            y = 0
            while len(self.body_email) == 0 and y <= 2:           # Try to find email, a maximum of three times
                y = y + 1
                print(f"Fetching email, attempt number: {y} -" + datetime.datetime.now().strftime("%H:%M:%S"))
                # Wait a minute for a new search
                self.wait(60000)
                self.body_email = self.page_activate_account.confirm_email()
                self.wait(10000)


            if len(self.body_email) == 0:
                print("Email Not Found, this execution was finish")
                # Entry log Failed, email not found
                self.maestro.new_log_entry(activity_label="sign_up_test",
                                           values={"timestamp": datetime.datetime.now().strftime("%d/%m/%Y-%H:%M"),
                                                   "conclusion": "FAIL", "note": "Email not found"})

                # Create alert "Email not found"
                self.maestro.alert(task_id=task_id, title="Email not found!",
                                   message="No email with subject was found 'Activate your BotCity account!'.",
                                   alert_type=AlertType.ERROR)


                # Send email alerting "Email not found"
                self.subject = "Test sign up - ERROR: Email not found"
                self.body = "No email with subject was found 'Activate your BotCity account!'."
                self.page_activate_account.send_email(self.subject, self.body)

                # Finish task
                self.maestro.finish_task(task_id=task_id, status=AutomationTaskFinishStatus.FAILED,
                                         message="Task Finished OK.")

            else:
                # Find data login BotMaestro and open the link
                self.link_activate_account = self.page_activate_account.collect_data_login()
                self.page_activate_account.activate_link()

                # Return login failed ou success
                self.login_failed = self.page_activate_account.result_login()

                if self.login_failed == "Failed":
                    print("login failed")
                    # Create a new log entry of login failed
                    self.maestro.new_log_entry(activity_label="sign_up_test",
                                               values={"timestamp": datetime.datetime.now().strftime("%d/%m/%Y-%H:%M"),
                                                       "conclusion": "FAIL", "note": f"Incorrect login data, see the "
                                                       f"difference between the registered login << {sign_up_test.credential.login} >> "
                                                       f"and the login used << {self.list_login[0]} >> and workspace "
                                                       f"registered << {sign_up_test.credential.workspace} >> "
                                                       f"and workspace used << {self.list_login[1]} >>"})

                    # Create alert login failed
                    self.maestro.alert(task_id=task_id, title="Login Failed",
                                       message="Check your execution report to verify that the data used to log in is the"
                                       " same as that recorded in the form", alert_type=AlertType.ERROR)


                    # Send email with alert login failed
                    self.subject = "Test sign up - ERROR: Login Failed"
                    self.body = "Check your execution report to verify that the data used to log in is the" \
                                " same as that recorded in the form'."
                    self.page_activate_account.send_email(self.subject, self.body)

                    # Finish task
                    self.maestro.finish_task(task_id=task_id, status=AutomationTaskFinishStatus.FAILED,
                                             message="Task Finished OK.")


                else:
                    print("Login completed successfully")
                    #If not error, complete task successfully in BotMaestro and delete account for next test

                    self.maestro.new_log_entry(activity_label="sign_up_test",
                                               values={"timestamp": datetime.datetime.now().strftime("%d/%m/%Y-%H:%M"),
                                                       "conclusion": "SUCCESS",
                                                       "note": "Login and test successfully terminated"})

                    # Finish Task
                    self.maestro.finish_task(task_id=task_id, status=AutomationTaskFinishStatus.SUCCESS,
                                             message="Task Finished OK.")


                    self.stop_browser()

        except Exception as e:
            print(e)

            # Send email with alert login failed
            self.subject = "Test sign up - ERROR: Login Failed"
            self.body = "Check your run report to verify that the maestro link has been opened for the login to run."
            self.page_activate_account.send_email(self.subject, self.body)




if __name__ == '__main__':
    TestSignUpGo.main()
